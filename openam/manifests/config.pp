# == Class: openam::config
#
# Module for initial configuration of ForgeRock OpenAM.
#
# === Authors
#
# Eivind Mikkelsen <eivindm@conduct.no>
#
# === Copyright
#
# Copyright (c) 2013 Conduct AS
#

class openam::config {
  #package { "perl-Crypt-SSLeay": ensure => installed }
 # package { "perl-libwww-perl": ensure => installed }

  file { "${openam::tomcat_home}/.openamcfg":
    ensure => directory,
    owner  => "${openam::tomcat_user}",
    group  => "${openam::tomcat_user}",
    mode   => 755,
  }

 file { "/opt/openam/config":
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => 755,
  }
 

  # Contains passwords, thus (temporarily) stored in /dev/shm
  file { "/opt/openam/config/configurator.properties":
    owner   => "root",
    group   => "root",
    mode    => 0755,
    content => template("${module_name}/configurator.properties.erb"),
  }

 file { "/opt/openam/config/SSOConfiguratorTools.zip":
    owner   => root,
    group   => root,
    mode    => 755,
    #require => File["puppet:///modules/openam/SSOConfiguratorTools.zip"], 
    source  => "puppet:///modules/openam/SSOConfiguratorTools.zip",
    notify => Exec["unzip"]
  }

  file { "${openam::config_dir}":
    ensure => directory,
    owner  => "${openam::tomcat_user}",
    group  => "${openam::tomcat_user}",
  }
  
  exec { "unzip":
	command => '/bin/unzip -o /opt/openam/config/SSOConfiguratorTools.zip -d /opt/openam/config/',
	path => '/opt/openam/config/',
	notify => Exec["rename"] 
  }
  exec { "rename":
	refreshonly => true,
	cwd => '/opt/openam/config/',
  	command => '/usr/bin/mv /opt/openam/config/openam-configurator-tool*.jar /opt/openam/config/openam-configurator-tool.jar',
  	path => ['/opt/openam/config/','/usr/bin'],
	creates => '/opt/openam/config/openam-configurator-tool.jar',
  	#owner   => root,
  	#group   => root,
  #      #mode    => 755,
       notify => File["/opt/openam/config/openam-configurator-tool.jar"]

  }
  file{ "/opt/openam/config/openam-configurator-tool.jar":
         #refreshonly => true,
	 owner   => root,
         group   => root,
         mode    => 755,
  }
  exec { "configure openam":
     # refreshonly => true,
      cwd => '/opt/openam/config/',
      #command => '/usr/bin/mv openam-configurator-tool-13.0.0-SNAPSHOT.jar openam-configurator-tool.jar',
      command => '/usr/bin/java -jar openam-configurator-tool.jar -f configurator.properties',
      path => ['/opt/openam/config/','/usr/bin/'],
      #require => [
      #File["${openam::config_dir}"],
      #File["/opt/openam/config/openam-configurator-tool.jar"]
    #],
    creates => "${openam::config_dir}/bootstrap",
    timeout => 600,
    logoutput => true,
    #notify => Service["${openam::tomcat_service}"]
    #timeout => 300
  }
service {'tomcat':
ensure => 'running',
enable => true,
}
}

